package es.eug.cfgs.ed.muxdemuxproject;

public class Muxdemux {
	/**
	 * 
	 * @param input
	 * @return
	 */
	public int convertNum(int input){
		return (int)(Math.pow(2, input)-1);
	}
	
	/**
	 * 
	 * @param input
	 * @return
	 */
	public String mux(int input){
		//Excepcio si input es negatiu
		if(input < 0){
			throw new NullPointerException("Entrada de metodo nula");
		}
		String res = "";
		while(input!=0&&input!=1){
			res = String.valueOf(input%2).concat(res);
			input/=2;
		}
		return  res;
	}
	
	/**
	 * 
	 * @param input
	 * @return
	 */
	public int demux(String input){
		int res=0;
		int j=0;
		for(int i=input.length();i>0;i--){
			/*Seguim aquesta formula per passar de binari a decimal
			 * 
			 *       0           1            0            0           1            0            1            0 
			 *    (0)*2^7  +  (1)*2^6   +  (0)*2^5  +   (0)*2^4  +  (1)*2^3   +  (0)*2^2   +  (1)*2^1   +  (0)*2^0 
			 *    
			 * Si seguim aquesta formula i la intentem extrapolar a la programaio, observarem que 
			 * hem de utilitzar dues variables. En el nostre cas utilitzem 'i' i 'j'. 
			 * 		'i'-> funciona com a index que indica el valor pel qual s'ha d'elevar a el 2
			 * 		'j'-> funciona com a cursor que va passant per cada element del numero binari
			 * Les dues variables recorren el rang de valors 0-input.length, en sentit invers.
			 *		
			 *	-Integer.parseInt(String.valueOf(input.charAt(j))
			 * Nomes agafa cada valor del input en forma de int.
			 *
			 */
			res+=(int)Math.pow(2,i-1)*Integer.parseInt(String.valueOf(input.charAt(j)));
			j++;
		}
		return res;
	}
}
