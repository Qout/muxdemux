package es.eug.cfgs.ed.muxdemuxproject;

import java.util.*;

public class Main {

	public static void main(String[] args) {
		
		Muxdemux m = new Muxdemux();
		Scanner lector = new Scanner(System.in);
		System.out.print("Introdueix un numero: ");
		String numb = "111101010";
		String numb1 = "001001";
		int resint = lector.nextInt();
		int inpint = m.convertNum(resint);
		System.out.println("Numero introduit ["+resint+"] convertit: "+inpint);
		System.out.println("Resultat del metode al passar "+inpint+" com a parametre "+m.mux(inpint)+"\n");
		System.out.println("El metode al qual pasem "+numb+" ens retorna "+m.demux(numb));
	}

}
