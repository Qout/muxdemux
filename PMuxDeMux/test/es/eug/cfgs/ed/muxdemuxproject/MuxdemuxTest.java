package es.eug.cfgs.ed.muxdemuxproject;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MuxdemuxTest {
	
	private static Muxdemux prueba;
	
	@Before
	public void setUp(){
		System.out.println("Configuración");
	}
	
	@After
	public void tearDown(){
		System.out.println("Limpieza");
	}
	
	@BeforeClass
	public static void setUpBeforeClass(){
		System.out.println("Configuración Inicial");
		prueba = new Muxdemux();
	}
	
	@AfterClass
	public static void tearDownAfterClass(){
		System.out.println("Limpieza General");
	}
	
	@Test
	public void testmux(){
		int[] inputs = {prueba.convertNum(3),prueba.convertNum(4),prueba.convertNum(2),prueba.convertNum(5)};
		String[] resultats = {"11","100","10","101"};
		for(int i=0;i>inputs.length;i++){
			try{
				assertEquals("Salida incorrecta",prueba.mux(inputs[i]),resultats[i]);
			}
			catch(NullPointerException ne){
				if(ne.getMessage().equals("Entrada de metodo nula")){
					assertTrue(true);
				}else{
					fail("Excepcion diferente a la esperada");
				}
			}
		}
	}
	
	@Test
	public void testdemux(){
		int[] resultats = {prueba.convertNum(3),prueba.convertNum(4),prueba.convertNum(2),prueba.convertNum(5)};
		String[] inputs = {"11","100","10","101"};
		for(int i=0;i>inputs.length;i++){
			try{
				assertEquals("Salida incorrecta",prueba.demux(inputs[i]),resultats[i]);
			}
			catch(NullPointerException ne){
				if(ne.getMessage().equals("Entrada de metodo nula")){
					assertTrue(true);
				}else{
					fail("Excepcion diferente a la esperada");
				}
			}
		}
	}
	

}
